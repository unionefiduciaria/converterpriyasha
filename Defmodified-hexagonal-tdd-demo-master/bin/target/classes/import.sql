
-- INSERT INTO hist_ex_rate VALUES ('CHF', '2020-01-01', 0.90);
-- INSERT INTO hist_ex_rate VALUES ('CHF', '2020-02-01', 1.05);
-- INSERT INTO hist_ex_rate VALUES ('USD', '2020-01-01', 0.88);
-- INSERT INTO hist_ex_rate VALUES ('USD', '2020-02-01', 0.88);

INSERT INTO hist_ex_rate VALUES ('CHF', '2020-11-30', 3.675);
INSERT INTO hist_ex_rate VALUES ('USD', '2020-11-30', 2.43);
INSERT INTO hist_ex_rate VALUES ('CHF', '2020-11-14', 6.52);
INSERT INTO hist_ex_rate VALUES ('USD', '2020-11-14', 7.91);
INSERT INTO hist_ex_rate VALUES ('CHF', '2020-11-18', 1.49);
INSERT INTO hist_ex_rate VALUES ('USD', '2020-11-09', 4.12);
INSERT INTO hist_ex_rate VALUES ('CHF', '2020-10-24', 8.31);
INSERT INTO hist_ex_rate VALUES ('USD', '2020-10-24', 5.88);
