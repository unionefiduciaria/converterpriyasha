package it.ff.hexagonal.tdd.histexrate;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.ff.hexagonal.tdd.histexrate.codegen.api.ConvertToApiDelegate;
import it.ff.hexagonal.tdd.histexrate.codegen.model.ExchangeRate;
import it.ff.hexagonal.tdd.histexrate.codeimpl.adapters.api.ConvertToApiDelegateImpl;
import it.ff.hexagonal.tdd.histexrate.codeimpl.application.port.ConvertAmountUseCase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import org.springframework.test.web.servlet.ResultActions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
class ConvertAmountWebTest {

    @Autowired
    private MockMvc mockMvc;          //oggetto finte chiamate

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean               //injects Mockito Mock
    private ConvertAmountUseCase ConvertAmountUseCase;

    @TestConfiguration
    static class AdditionalConfiguration {
        @Bean
        public ConvertToApiDelegate getConvertToApiDelegate() {
            return new ConvertToApiDelegateImpl();
        }
    }

    @Test
    public void should_retrieve_ConvertedAmount_when_valid_request() throws Exception {

//        List<ExchangeRate> l = new ArrayList<>();
//        l.add(new ExchangeRate().rate(BigDecimal.valueOf(0.90)).currency("USD").date(LocalDate.parse("2020-11-30")));
//        l.add(new ExchangeRate().rate(BigDecimal.valueOf(0.84)).currency("CHF").date(LocalDate.parse("2020-11-30")));

//        when(ConvertAmountUseCase.convert("USD","CHF",BigDecimal.valueOf(200), LocalDate.parse("2020-11-30"))).thenReturn(l);
//
//        mockMvc.perform(get("/convert-to?amount={amount}&currency_from={currencyFrom}&currency_to={currencyTo}","200","USD", "CHF")
//                .accept(MediaType.APPLICATION_JSON)
//                .andExpect(status().isOk()));
//
//
//

    }
}














//        ConvertedAmount ca = new ConvertedAmount();
//        List<ExchangeRate> ex = null;
//        ca.currency("USD").exchangeRates();
//        ca.currency("USD").exchangeRates(ex.rate(BigDecimal.valueOf(0.80))).amount(100);
//        ConvertedAmount convertedAmount = new ConvertedAmount().amount(BigDecimal.valueOf(100)).currency("USD").exchangeRates(new )