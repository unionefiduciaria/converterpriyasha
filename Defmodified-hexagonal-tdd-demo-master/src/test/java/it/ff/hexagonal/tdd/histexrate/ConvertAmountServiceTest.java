package it.ff.hexagonal.tdd.histexrate;

import it.ff.hexagonal.tdd.histexrate.codegen.model.ConvertedAmount;
import it.ff.hexagonal.tdd.histexrate.codegen.model.ExchangeRate;
import it.ff.hexagonal.tdd.histexrate.codeimpl.application.port.HistoricalExchangeRatesPort;
import it.ff.hexagonal.tdd.histexrate.codeimpl.application.service.ConvertAmountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
//import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

//import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.time.LocalDate;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.List;

//import static java.lang.reflect.Array.*;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
//import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith({SpringExtension.class})
public class ConvertAmountServiceTest {

    @InjectMocks
    private ConvertAmountService ConvertAmountService;

    @MockBean       //injects Mockito Mock
    private HistoricalExchangeRatesPort HistoricalExchangeRatesPort;

    @BeforeEach     //executed before each test (used to prepare test environment)
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void convert_returnObjectConverted() {

//        List<ExchangeRate> l = new ArrayList<>();
        ExchangeRate c = new ExchangeRate().rate(BigDecimal.valueOf(0.90)).currency("USD").date(LocalDate.parse("2020-11-30"));
        ExchangeRate ct = new ExchangeRate().rate(BigDecimal.valueOf(0.75)).currency("CHF").date(LocalDate.parse("2020-11-30"));
        when(HistoricalExchangeRatesPort.findAllByIdentity_idCurrencyOrIdentity_idDate("USD", "CHF", LocalDate.parse("2020-11-30")))
                .thenReturn(asList(c, ct));

        ConvertedAmount ca = ConvertAmountService.convert("USD", "CHF", BigDecimal.valueOf(100), LocalDate.parse("2020-11-30"));

        assertThat(ca.getExchangeRates().size()).isEqualTo(2);
        assertThat(ca.getExchangeRates().get(0).currency("USD"));
        assertThat(ca.getExchangeRates().get(0).date(LocalDate.parse("2020-11-30")));
        assertThat(ca.getExchangeRates().get(1).currency("CHF"));
        assertThat(ca.getExchangeRates().get(1).date(LocalDate.parse("2020-11-30")));
        assertThat(ca.amount(BigDecimal.valueOf(120)));

    }

    @Test
    void convert_withDate() {

        ExchangeRate c  = new ExchangeRate().rate(BigDecimal.valueOf(2.70)).currency("AUD").date(LocalDate.parse("2020-01-12"));
        ExchangeRate c1 = new ExchangeRate().rate(BigDecimal.valueOf(1.30)).currency("TRY").date(LocalDate.parse("2020-01-12"));

        when(HistoricalExchangeRatesPort.findAllByIdentity_idCurrencyOrIdentity_idDate("AUD", "CHF", LocalDate.parse("2020-01-12")))
                .thenReturn(asList(c, c1));

        ConvertedAmount ca = ConvertAmountService.convert("AUD", "CHF", BigDecimal.valueOf(100), LocalDate.parse("2020-01-12"));

        assertThat(ca.getExchangeRates().size()).isEqualTo(2);
        assertThat(ca.getExchangeRates().get(0).currency("AUD"));
        assertThat(ca.getExchangeRates().get(1).currency("CHF"));
        assertThat(ca.getExchangeRates().get(0).getDate().isEqual(LocalDate.parse("2020-01-12")));
        assertThat(ca.getExchangeRates().get(1).getDate().isEqual(LocalDate.parse("2020-01-12")));

    }


    @Test
    public void convert_with_NoDate() {

        ExchangeRate c  = new ExchangeRate().rate(BigDecimal.valueOf(2.70)).currency("AUD").date(LocalDate.parse("2020-01-12"));
        ExchangeRate c1 = new ExchangeRate().rate(BigDecimal.valueOf(1.30)).currency("CHF").date(LocalDate.parse("2020-01-12"));

        when(HistoricalExchangeRatesPort.findAllByIdentity_idCurrency("AUD", "CHF"))
                .thenReturn(asList(c, c1));

        ConvertedAmount ca = ConvertAmountService.convert("AUD", "CHF", BigDecimal.valueOf(100), null);

        assertThat(ca.getExchangeRates().size()).isEqualTo(2);
        assertThat(ca.getExchangeRates().get(0).currency("AUD"));
        assertThat(ca.getExchangeRates().get(1).currency("CHF"));
        assertThat(ca.getExchangeRates().get(0).getDate().isEqual(LocalDate.parse("2020-01-12")));
        assertThat(ca.getExchangeRates().get(1).getDate().isEqual(LocalDate.parse("2020-01-12")));

    }

}