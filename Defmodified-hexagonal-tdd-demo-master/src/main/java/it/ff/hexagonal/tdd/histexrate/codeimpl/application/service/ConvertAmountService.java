package it.ff.hexagonal.tdd.histexrate.codeimpl.application.service;

import it.ff.hexagonal.tdd.histexrate.codegen.model.ConvertedAmount;
import it.ff.hexagonal.tdd.histexrate.codegen.model.ErrorDetail;
import it.ff.hexagonal.tdd.histexrate.codegen.model.ExchangeRate;
import it.ff.hexagonal.tdd.histexrate.codeimpl.application.exception.NotFoundException;
import it.ff.hexagonal.tdd.histexrate.codeimpl.application.exception.ServiceUnavailableException;
import it.ff.hexagonal.tdd.histexrate.codeimpl.application.port.ConvertAmountUseCase;
import it.ff.hexagonal.tdd.histexrate.codeimpl.application.port.HistoricalExchangeRatesPort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class ConvertAmountService implements ConvertAmountUseCase {

    @Autowired
    private HistoricalExchangeRatesPort historicalExchangeRatesPort;

    @Override
    public ConvertedAmount convert(String currencyFrom, String currencyTo, BigDecimal amount, LocalDate date) {

        ConvertedAmount ca = new ConvertedAmount();
        List<ErrorDetail> errorListNF = new ArrayList<>();

        try {

            List<ExchangeRate> cambi;
            ExchangeRate from;
            ExchangeRate to;

            if (date != null) {
                cambi = historicalExchangeRatesPort.findAllByIdentity_idCurrencyOrIdentity_idDate(currencyFrom, currencyTo, date);
            } else {
                cambi = historicalExchangeRatesPort.findAllByIdentity_idCurrency(currencyFrom, currencyTo);
            }

            if (cambi.size() < 2) {
                errorListNF.add(new ErrorDetail("Currency", "null", "Currency not found in the Historical Exchange"));
                                        //error, which type of currency?
            }
            else {
                from = cambi.get(0);
                to = cambi.get(1);

                ca.addExchangeRatesItem(from);
                ca.addExchangeRatesItem(to);

                ca.setCurrency(currencyTo);

                BigDecimal u = from.getRate();
                BigDecimal c = to.getRate();

                BigDecimal total = (u.divide(c, RoundingMode.HALF_UP).multiply(amount));

                ca.setAmount(total);
            }

            if (!errorListNF.isEmpty()) {
                throw new NotFoundException(errorListNF);
            }
        }

        catch (ServiceUnavailableException | NotFoundException e) {
            throw e;
        } catch (Exception ex) {
            throw new ServiceUnavailableException(ex);
        }

        return ca;
    }
}
