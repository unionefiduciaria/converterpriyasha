package it.ff.hexagonal.tdd.histexrate.codeimpl.adapters.persistence;

import it.ff.hexagonal.tdd.histexrate.codegen.model.ExchangeRate;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Currency;
import java.util.List;

public interface HistExRateRepository extends PagingAndSortingRepository<HistExRate, HistExRateIdentity> {

//    @Query(value = "SELECT TOP 1 s.* "
//            + "FROM	HistExRate s "
//            + "WHERE "
//            + "currencyCode	= ?1 AND	dateRate <= ?2 "
//            + "ORDER BY	dateRate DESC", nativeQuery = true)
//    public HistExRate getCambioAtDate(String div, LocalDate date);

    List<HistExRate> findAllByIdentity_idCurrencyIgnoreCase(String currency, Pageable p);

    List<HistExRate> findAllByIdentity_idDate(LocalDate dateRate, Pageable p);

    List<HistExRate> findAllByIdentity_idCurrencyAndIdentity_idDate(String currency, LocalDate date, Pageable p);



//TODO    Converted Amount
    List<HistExRate> findAllByIdentity_idCurrency(String currency);

    HistExRate findAllByIdentity_idCurrencyAndIdentity_idDate(String currency, LocalDate date);


//    @Query(value = "SELECT s.*"
//            + "FROM HistExRate s"
//            + "WHERE"
//            + "idCurrency = ?1 OR idCurrency = ?2 AND idDate = ?3", native query = true)
//    List<HistExRate> findAllByIdentity_idCurrencyOrIdentity_idCurrencyAndIdentity_idDate(String currencyFrom, String currencyTo, LocalDate date);

    @Query(value ="SELECT * FROM HIST_EX_RATE "
        + "WHERE ID_CURRENCY = ?1 OR ID_CURRENCY = ?2 "
        + "GROUP BY ID_DATE, ID_CURRENCY "
        + "HAVING ID_DATE = ?3 " ,nativeQuery = true)
List<HistExRate> findAllByIdentity_idCurrencyOrIdentity_idCurrencyAndIdentity_idDate(String currencyFrom, String currencyTo, LocalDate date);


    @Query(value ="SELECT TOP 2 v1.* FROM HIST_EX_RATE AS v1 "
           +"JOIN HIST_EX_RATE AS v2 ON v1.ID_DATE = v2.ID_DATE "
           +"WHERE (v1.ID_CURRENCY = ?1 OR v1.ID_CURRENCY = ?2) "
           +"AND (v2.ID_CURRENCY = ?1 OR v2.ID_CURRENCY = ?2) "
           +"AND v1.ID_CURRENCY <> v2.ID_CURRENCY "
           +"GROUP BY  v1.ID_DATE, v1.ID_CURRENCY "
           +"ORDER BY ID_DATE DESC ", nativeQuery = true)
    List<HistExRate>findAllByIdentity_idCurrencyOrIdentity_idCurrencyOrderByIdentity_idDateDesc(String currencyFrom,String currencyTo);


    @Transactional
    @Modifying
    @Query(value = "UPDATE HistExRate SET rate=:#{#stc.rate} WHERE id_currency=:#{#stc.identity.idCurrency} AND id_date=:#{#stc.identity.idDate}")
    int updateHistExRate(@Param("stc") HistExRate HistoricalExchangeRateso);
}





















//    SELECT TOP 2 v1.* FROM HIST_EX_RATE AS v1
//    JOIN HIST_EX_RATE AS v2 ON v1.ID_DATE = v2.ID_DATE
//    WHERE (v1.ID_CURRENCY = 'USD' OR v1.ID_CURRENCY = 'CHF')
//    AND (v2.ID_CURRENCY = 'USD' OR v2.ID_CURRENCY = 'CHF')
//    AND v1.ID_CURRENCY <> v2.ID_CURRENCY
//    GROUP BY  v1.ID_DATE, v1.ID_CURRENCY
//    ORDER BY ID_DATE DESC





//    List<HistExRate> findAllByIdentity_idCurrencyOrIdentity_idCurrencyOrderByIdentity_idDateDesc(String currencyFrom, String currencyTo);

//    List<HistExRate> findAllByIdentity_idCurrencyAndIdentity_idCurrencyOrderByIdentity_idCurrencyDesc(String currencyFrom, String currencyTo);

//@Query(value = "SELECT * FROM ", nativeQuery = true)
//List<HistExRate> findAllByIdentity_idCurrencyOrIdentity_idCurrencyAndIdentity_idDate(String currencyFrom, String currencyTo, LocalDate date);



//    @Query(value = "SELECT * FROM EVENTS_TABLE\n" +
//            "where date_time between cast(:dateFrom AS timestamp) AND cast(:dateTo AS timestamp)", nativeQuery = true)
//    Events[] findAllEventsBetweenDate(@Param("dateTo")String dateTo, @Param("dateFrom")String dateFrom);