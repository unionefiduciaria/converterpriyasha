package it.ff.hexagonal.tdd.histexrate.codeimpl.application.exception;

import it.ff.hexagonal.tdd.histexrate.codegen.model.ErrorDetail;

import java.util.List;

public class BadRequestException extends RuntimeException {

    //agg
    public List<ErrorDetail> errorDetail;

    public List<ErrorDetail> getErrorDetail() {
        return errorDetail;
    }

    public BadRequestException(String message) {
        super(message);
    }


    public BadRequestException (List<ErrorDetail> errorDetail) {
        this.errorDetail = errorDetail;
    }

}
