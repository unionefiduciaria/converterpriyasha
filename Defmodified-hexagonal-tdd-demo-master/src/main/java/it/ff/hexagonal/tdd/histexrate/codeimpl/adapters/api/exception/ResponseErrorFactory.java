package it.ff.hexagonal.tdd.histexrate.codeimpl.adapters.api.exception;

import it.ff.hexagonal.tdd.histexrate.codegen.model.ErrorDetail;
import it.ff.hexagonal.tdd.histexrate.codegen.model.ResponseError;

import java.util.List;

/**
 * Build the error object used in response object
 */
public class ResponseErrorFactory {
    private ResponseErrorFactory() {
    }

    public static ResponseError create(String code, String description) {
        ResponseError responseError = new ResponseError();
        responseError.setCode(code);
        responseError.setDescription(description);

        return responseError;
    }

    //aggiunto
    public static ResponseError create(String code, String description, List<ErrorDetail> errorDetList) {
        ResponseError responseError = new ResponseError();
        responseError.setCode(code);
        responseError.description(description);
        responseError.errorList(errorDetList);

        return responseError;
    }


}
