package it.ff.hexagonal.tdd.histexrate.codeimpl.application.port;

import it.ff.hexagonal.tdd.histexrate.codegen.model.ConvertedAmount;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface ConvertAmountUseCase {

    //mi restituisce l'oggetto convertito
    ConvertedAmount convert (String currencyFrom,
                             String currencyTo,
                             BigDecimal amount,
                             LocalDate date);

}
