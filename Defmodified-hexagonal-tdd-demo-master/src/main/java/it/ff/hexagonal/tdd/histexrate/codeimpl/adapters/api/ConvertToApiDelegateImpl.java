package it.ff.hexagonal.tdd.histexrate.codeimpl.adapters.api;

import it.ff.hexagonal.tdd.histexrate.codegen.api.ConvertToApiDelegate;
import it.ff.hexagonal.tdd.histexrate.codegen.model.ConvertedAmount;
import it.ff.hexagonal.tdd.histexrate.codegen.model.ErrorDetail;
//import it.ff.hexagonal.tdd.histexrate.codegen.model.ExchangeRate;
//import it.ff.hexagonal.tdd.histexrate.codegen.model.ResponseError;
//import it.ff.hexagonal.tdd.histexrate.codeimpl.adapters.persistence.HistoricalExchangeRatesRepository;
import it.ff.hexagonal.tdd.histexrate.codeimpl.application.exception.BadRequestException;
import it.ff.hexagonal.tdd.histexrate.codeimpl.application.exception.GenericServiceException;
import it.ff.hexagonal.tdd.histexrate.codeimpl.application.exception.NotFoundException;
//import it.ff.hexagonal.tdd.histexrate.codeimpl.application.exception.ServiceUnavailableException;
import it.ff.hexagonal.tdd.histexrate.codeimpl.application.port.ConvertAmountUseCase;
//import it.ff.hexagonal.tdd.histexrate.codeimpl.application.port.HistoricalExchangeRatesPort;
//import org.junit.platform.commons.function.Try;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class ConvertToApiDelegateImpl implements ConvertToApiDelegate {


    @Autowired      //definisce dipendenze tra Bean (inietto..UseCase)
    ConvertAmountUseCase convertAmountUseCase;

    @Override
    public ResponseEntity<ConvertedAmount> getConvertedAmount(String currencyFrom,
                                                              String currencyTo,
                                                              BigDecimal amount,
                                                              LocalDate date) {

        List<ErrorDetail> errorListBR = new ArrayList<>();
        List<ErrorDetail> errorListNF = new ArrayList<>();

        new ConvertedAmount();
        ConvertedAmount response;

        try {

            if (currencyFrom.equals(currencyTo)) {
                errorListBR.add(new ErrorDetail("currency From & currency To", "Same values", "Currency From is equal to currency To."));
            }

            if(StringUtils.isEmpty(currencyFrom)) {
                errorListNF.add(new ErrorDetail("Currency From", "empty", "Currency From not found."));
            }

            if (StringUtils.isEmpty(currencyTo)) {
                errorListNF.add(new ErrorDetail("Currency To", " null - empty", "Currency To not found."));
            }

            if (currencyFrom.length() != 3) {
                errorListBR.add(new ErrorDetail("Currency From", "Length", "Currency From must have a length of 3 characters."));
            }

            if (currencyTo.length() != 3) {
                errorListBR.add(new ErrorDetail("Currency To", "Length", "Currency To must have a length of 3 characters."));
            }

            if (!errorListBR.isEmpty()) {
                throw new BadRequestException(errorListBR);
            }

            if (!errorListNF.isEmpty()) {
                throw new NotFoundException(errorListNF);
            }

            response = convertAmountUseCase.convert(currencyFrom, currencyTo, amount, date);
        }
        catch (NotFoundException | BadRequestException ex) {
            throw ex;
        }
        catch (EmptyResultDataAccessException x) {
            throw new NotFoundException(x);
        } catch (Exception e) {
            throw new GenericServiceException(e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
