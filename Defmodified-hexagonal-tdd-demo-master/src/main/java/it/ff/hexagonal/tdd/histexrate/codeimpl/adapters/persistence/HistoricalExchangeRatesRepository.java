package it.ff.hexagonal.tdd.histexrate.codeimpl.adapters.persistence;

import it.ff.hexagonal.tdd.histexrate.codegen.model.ExchangeRate;
import it.ff.hexagonal.tdd.histexrate.codeimpl.application.port.HistoricalExchangeRatesPort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class HistoricalExchangeRatesRepository implements HistoricalExchangeRatesPort {

    @Autowired
    private HistExRateRepository repository;

    @Override
    public List<ExchangeRate> findAll(Integer offset, Integer limit) {
        Pageable p = PageRequest.of(offset, limit);
        return mappertToExchangeRate(repository.findAll(p));
    }

    @Override
    public List<ExchangeRate> findAllByIdentity_idCurrency(String currency, Integer offset, Integer limit) {
        Pageable p = PageRequest.of(offset, limit);
        return mappertToExchangeRate(repository.findAllByIdentity_idCurrencyIgnoreCase(currency, p));
    }

    @Override
    public List<ExchangeRate> findAllByIdentity_idDate(LocalDate date, Integer offset, Integer limit) {
        Pageable p = PageRequest.of(offset, limit);
        return mappertToExchangeRate(repository.findAllByIdentity_idDate(date, p));
    }

    @Override
    public List<ExchangeRate> findAllByIdentity_idCurrencyAndIdentity_idDate(String currency, LocalDate date, Integer offset, Integer limit) {
        Pageable p = PageRequest.of(offset, limit);
        return mappertToExchangeRate(repository.findAllByIdentity_idCurrencyAndIdentity_idDate(currency, date, p));
    }

    @Override
    public ExchangeRate save(ExchangeRate ExchangeRate) {
        return mapperToExchangeRate(repository.save(mapperToHistExRate(ExchangeRate)));
    }

    @Override
    public int updateHistExRate(ExchangeRate ExchangeRate) {
        return repository.updateHistExRate(mapperToHistExRate(ExchangeRate));
    }

    @Override
    public void deleteHistExRate(String currency, LocalDate data) {
        repository.deleteById(new HistExRateIdentity(currency, data));
    }

//     public ExchangeRate findAllByIdentityCF_idCurrency(String currencyFrom, LocalDate date) {
//        HistExRate p = repository.findAllByIdentity_idCurrencyAndIdentity_idDate(currencyFrom, date);
//        ExchangeRate e = mapperToExchangeRate(p);
//        return e;
//    }

    //TODO:
    // recupero il tasso di cambio at data
    @Override
    public List<ExchangeRate> findAllByIdentity_idCurrencyAndIdentity_idDate(String currencyFrom, String currencyTo, LocalDate date) {
        return mappertToExchangeRate(repository.findAllByIdentity_idCurrencyOrIdentity_idCurrencyAndIdentity_idDate(currencyFrom, currencyTo, date));
//        return mapperToExchangeRate(repository.findAllByIdentity_idCurrencyOrIdentity_idCurrencyAndIdentity_idDate(currencyFrom, currencyTo, date));
   }
//mapperToExchangeRate(repository.findAllByIdentity_idCurrencyOrIdentity_idCurrencyAndIdentity_idDate(currencyFrom, currencyTo, date));


    //TODO:
    // recupero il tasso di cambio at last date available
    @Override
    public List<ExchangeRate> findAllByIdentity_idCurrency(String currencyFrom, String currencyTo) {
//        LocalDate date = LocalDate.now();
        return mappertToExchangeRate(repository.findAllByIdentity_idCurrencyOrIdentity_idCurrencyOrderByIdentity_idDateDesc(currencyFrom, currencyTo));
    }
//mapperToExchangeRate(repository.findAllByIdentity_idCurrencyOrIdentity_idCurrencyOrderByIdentity_idDateDesc(currencyFrom, currencyTo));


    //TODO: prova3
    // no data inserita: mi trova il tasso di entrambe con la stessa data, la più recente che hanno
//    public List<ExchangeRate> findAllByIdentityS_idCurrencyAndIdentity_idDate(String currencyFrom, String currencyTo, LocalDate date) {
//        return (List<ExchangeRate>) mapperToExchangeRate((HistExRate) repository.findAllByIdentity_idCurrencyAndIdentity_idCurrencyOrderByIdentity_idCurrencyDesc(currencyFrom, currencyTo));
//    }
//mapperToExchangeRate(repository.findDistinctByIdentity_idCurrencyAndIdentity_idCurrencyOrderByIdentity_idDateDesc(currencyFrom, currencyTo));



    private ExchangeRate mapperToExchangeRate(HistExRate histExRate) {
        ExchangeRate cs = null;
        if (histExRate != null && histExRate.getIdentity() != null && histExRate.getIdentity().getIdCurrency() != null) {
            cs = new ExchangeRate();
            cs.setRate(histExRate.getRate());
            cs.setDate(histExRate.getIdentity().getIdDate());
            cs.setCurrency(histExRate.getIdentity().getIdCurrency());
        }
        return cs;
    }


//    recupero il tasso di cambio
    private List<ExchangeRate> mappertToExchangeRate(Iterable<HistExRate> i) {
        List<ExchangeRate> res = new ArrayList<>();

        for (HistExRate HistExRate : i) {
            res.add(mapperToExchangeRate(HistExRate));
        }

        return res;
    }
//    scrive il tasso di cambio
    private HistExRate mapperToHistExRate(ExchangeRate cs) {
        HistExRate e = new HistExRate();
        e.setRate(cs.getRate());
        HistExRateIdentity identity = new HistExRateIdentity(cs.getCurrency(), cs.getDate());
        e.setIdentity(identity);
        return e;
    }


//    recupero la currencyFrom e la data
    @Override
    public ExchangeRate findAllByIdentityCF_idCurrency(String currencyFrom, LocalDate date) {
        HistExRate p = repository.findAllByIdentity_idCurrencyAndIdentity_idDate(currencyFrom, date);
        ExchangeRate e = mapperToExchangeRate(p);
        return e;
    }

//   recupero la currencyTo e la data
    @Override
    public ExchangeRate findAllByIdentityCT_idCurrency(String currencyTo, LocalDate date) {
        HistExRate s = repository.findAllByIdentity_idCurrencyAndIdentity_idDate(currencyTo, date);
        ExchangeRate m = mapperToExchangeRate(s);
        return m ;
    }


//  Recupero il tasso di cambio alla data inserita
    @Override
    public ExchangeRate findAllByIdentity_idCurrency_idDate(String currency, LocalDate date) {
        return mapperToExchangeRate(repository.findAllByIdentity_idCurrencyAndIdentity_idDate(currency, date));
    }

//  Recupero la valuta alla data più recente
   @Override
    public ExchangeRate findAllByIdentity_idCurrency(String currency) {

        List<ExchangeRate> currencyList = mappertToExchangeRate(repository.findAllByIdentity_idCurrency(currency));

        ExchangeRate it = new ExchangeRate();

        for (ExchangeRate exchange: currencyList) {

            if (LocalDate.now().minusDays(1).isEqual(exchange.getDate())) {
                it = exchange;
                break;
            }
            else if (it.getDate() == null){
                it = exchange;
            }
            else if (exchange.getDate().isAfter(it.getDate())) {
                it = exchange;
            }
        }
       return it;
   }

    @Override
    public List<ExchangeRate> findAllByIdentity_idCurrencyOrIdentity_idDate(String currencyFrom, String currencyTo, LocalDate date) {
//        LocalDate dt = LocalDate.now();
        return mappertToExchangeRate(repository.findAllByIdentity_idCurrencyOrIdentity_idCurrencyAndIdentity_idDate(currencyFrom, currencyTo, date));
    }


}







//   exchange.getDate().isAfter(it.getDate())

//        for (int i = 0; i <= currencyList.size(); i++) {
//
//            if (it.getDate().isEqual(LocalDate.now().minusDays(1))) {
//                it = currencyList.get(i);
//                break;
//            }
//            else {
//                if (currencyList.get(i).getDate().isAfter(it.getDate())) {
//                    it = currencyList.get(i);
//                }
//            }
//        }



